#ifndef _HAV_CONTROL_LIB_H_
#define _HAV_CONTROL_LIB_H_

#include <mk_model/mk_common.h>
#include <ca_common/math.h>
#include <ros/node_handle.h>


namespace CA
{

  class HavControlParameters
  {
  public:

    bool doVelocityControl;
    bool doHeadingControl;
    bool doThrustControl;
    
    double minAttitudeCmd;
    double maxAttitudeCmd;
    
    double minYawCmd;
    double maxYawCmd;
    
    double minThrustCmd;
    double maxThrustCmd;
    
    double velocityFF;
    double velocityP;
    double velocityI;
    double velocityD;
    double velocityIMax;
    double attitudeGain;
    
    double headingP;
    double headingrateP;
    double thrustFF;
    double mass;
    double thrustP;
    double thrustI;
    double thrustD;
    double thrustIMax;
    double thrustCurveOffset;
    double thrustCurveFactor;

    double airDensity;

    HavControlParameters():
      doVelocityControl(false),
      doHeadingControl(false),
      doThrustControl(false),
      minAttitudeCmd(0.0),
      maxAttitudeCmd(0.0),    
      minYawCmd(0.0),
      maxYawCmd(0.0),
      minThrustCmd(150.0),
      maxThrustCmd(150.0),
      velocityFF(0.0),
      velocityP(0.0),
      velocityI(0.0),
      velocityD(0.0),
      velocityIMax(1.0),
      attitudeGain(1.0),
      headingP(0.0),
      headingrateP(0.0),
      thrustFF(0.0),
      mass(1.0),
      thrustP(0.0),
      thrustI(0.0),
      thrustD(0.0),
      thrustIMax(10.0),
      thrustCurveOffset(20.0),
      thrustCurveFactor(1.0/0.3209),
      airDensity(1.0)
    {}
    bool loadParameters(ros::NodeHandle &n)
    {
      bool found = true;
      //
      // Parameters to enable/disable sections of the controller
      //
      found = found && n.getParam("controlVelocity", doVelocityControl);
      found = found && n.getParam("controlHeading", doHeadingControl);
      found = found && n.getParam("controlThrust", doThrustControl);
          //
      // Parameters to set the gains on the controllers
      //
      found = found && n.getParam("velocityFF", velocityFF);
      found = found && n.getParam("velocityP", velocityP);
      found = found && n.getParam("velocityI", velocityI);
      found = found && n.getParam("velocityD", velocityD);
      found = found && n.getParam("velocityIMax", velocityIMax);
     
      found = found && n.getParam("attitudeGain", attitudeGain);
      found = found && n.getParam("headingrateP", headingrateP);
      found = found && n.getParam("headingP", headingP);
    
      found = found && n.getParam("thrustFF", thrustFF);
      found = found && n.getParam("mass", mass);
      found = found && n.getParam("thrustP", thrustP);
      found = found && n.getParam("thrustI", thrustI);
      found = found && n.getParam("thrustD", thrustD);
      found = found && n.getParam("thrustIMax", thrustIMax);     

      found = found && n.getParam("thrustCurveOffset", thrustCurveOffset);     
      found = found && n.getParam("thrustCurveFactor", thrustCurveFactor);     
      //
      // Parameters to set the allowable range for each command
      //

      found = found && n.getParam("minAttitudeCmd", minAttitudeCmd);
      found = found && n.getParam("maxAttitudeCmd", maxAttitudeCmd); 
      found = found && n.getParam("minYawCmd", minYawCmd);
      found = found && n.getParam("maxYawCmd", maxYawCmd);  
      found = found && n.getParam("minThrustCmd", minThrustCmd);
      found = found && n.getParam("maxThrustCmd", maxThrustCmd);
      found = found && n.getParam("airDensity", airDensity);
      return found;
    }

  };

  class HavControl
  {
  public:
	bool debug;
    HavControlParameters pr;
    ros::Publisher pubDebugVX,pubDebugVY,pubDebugThrust;
    HavControl(    HavControlParameters para=    HavControlParameters()):
      pr(para)
    {}
    MkModelLibCommand velocityControl(double dt,  CA::QuatD orientation, MkVelocityControlState &ctState,MkVelocityControlCommand actual,MkVelocityControlCommand command,MkVelocityControlCommand prev_command);
    MkModelLibCommand levelCommand(void);
    bool validVelocityRange(double OdomVelocityRange,double CommandVelocityRange,MkVelocityControlCommand actual,MkVelocityControlCommand desired);
	void debugInit(ros::NodeHandle &n);

    double calcDensityDownPull(double airDensity)
    {
    	return (std::max(0.5,std::min(1.42,airDensity)) - 0.7) * ( -10.0 ) ;
    }

  };


}

#endif
