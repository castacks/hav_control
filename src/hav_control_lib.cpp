
#include <mk_model/mk_common.h>
#include <hav_control/hav_control_lib.h>
#include <ros/ros.h>
#include <ca_common/math.h>
#include <ca_common/debugPIDcomponents.h>

using namespace CA;
bool CA::HavControl:: validVelocityRange(double OdomVelocityRange,double CommandVelocityRange,MkVelocityControlCommand actual,MkVelocityControlCommand desired)
{
    bool status = true;
    if(OdomVelocityRange>0)
    {
        if((actual.velocity[0] > OdomVelocityRange)||(actual.velocity[0] < -OdomVelocityRange)) status=false;
        if((actual.velocity[1] > OdomVelocityRange)||(actual.velocity[1] < -OdomVelocityRange)) status=false;
        if((actual.velocity[2] > OdomVelocityRange)||(actual.velocity[2] < -OdomVelocityRange)) status=false;
    }
    if(status==false)
    {
        ROS_ERROR_STREAM_THROTTLE(2,"HAV_control: Velocity from odometery is too High (limit "<<OdomVelocityRange<< " odom "<<actual.velocity<<" )");
        return status;
    }
    if(CommandVelocityRange>0)
    {
        if((desired.velocity[0] > CommandVelocityRange)||(desired.velocity[0] < -CommandVelocityRange)) status=false;
        if((desired.velocity[1] > CommandVelocityRange)||(desired.velocity[1] < -CommandVelocityRange)) status=false;
        if((desired.velocity[2] > CommandVelocityRange)||(desired.velocity[2] < -CommandVelocityRange)) status=false;
    }
    if(status==false)
    {
        ROS_ERROR_STREAM_THROTTLE(2,"HAV_control: Velocity from command is too High (limit "<<CommandVelocityRange<< " odom "<<desired.velocity<<" )");
    }
    return status;
}

void CA::HavControl::debugInit(ros::NodeHandle &n)
{
	n.param("debug", debug, false);
    pubDebugVX = n.advertise<ca_common::debugPIDcomponents>("/hav_control/debug_pitchvx_pid", 50);
    pubDebugVY  = n.advertise<ca_common::debugPIDcomponents>("/hav_control/debug_rollvy_pid", 50);
    pubDebugThrust = n.advertise<ca_common::debugPIDcomponents>("/hav_control/debug_thrust_pid", 50);
}
MkModelLibCommand CA::HavControl::velocityControl(double dt,  CA::QuatD orientation, MkVelocityControlState &ctState,MkVelocityControlCommand actual,MkVelocityControlCommand command,MkVelocityControlCommand prev_command)
{
    MkModelLibCommand cr;
    //Check input invariants:
    if(!isfinite(orientation) || !ctState.isfinite() || !actual.isfinite() || !command.isfinite())
    {
        ROS_ERROR_STREAM_THROTTLE(1.0,"Received an invalid input to control. Sending 0's");
        return cr;
    }

    //
    // Yaw Control
    //
    if(pr.doHeadingControl)
    {

        double heading_err = math_tools::turnDirection(command.heading , actual.heading);
        cr.yaw = heading_err * pr.headingP + command.headingrate * pr.headingrateP;
    } else
    {
        ROS_WARN_STREAM("Heading control disabled!");
    }

    //
    // Thrust Control
    //
    if(pr.doThrustControl)
    {
        double zd_err = command.velocity[2] - actual.velocity[2];

        // Account for +z being down
        zd_err *= -1.0;

        ctState.vzIntegrator += zd_err * dt;
        ctState.vzIntegrator = std::max(ctState.vzIntegrator, -pr.thrustIMax);
        ctState.vzIntegrator = std::min(ctState.vzIntegrator,  pr.thrustIMax);


        //Convert this command to newtons:
        double downPull = CA::math_consts::GRAVITY * pr.mass;

        double vert_thrust_n = downPull +
                               pr.thrustFF * command.acceleration[2] * pr.mass +
                               pr.thrustP  * zd_err +
                               pr.thrustI  * ctState.vzIntegrator +
                               pr.thrustD  * (zd_err - ctState.prev_zd_err)/dt;
		if(debug)
		{
			ca_common::debugPIDcomponents debugPIDcomponents;
			debugPIDcomponents.p=pr.thrustP  * zd_err;
			debugPIDcomponents.i=pr.thrustI  * ctState.vzIntegrator;
			debugPIDcomponents.d=pr.thrustD  * (zd_err - ctState.prev_zd_err)/dt;
			pubDebugThrust.publish(debugPIDcomponents);
		}
        ctState.prev_zd_err = zd_err;

        // Convert command to force, account for attitude

        CA::Vector3D world_thrust_n(0,0,vert_thrust_n);
        CA::Vector3D down(0.0,0.0,1.0);
        CA::Vector3D rotated = orientation.inverse() * down;
        double compFactor = std::max(0.173,std::min(1.0,rotated[2])); //compFactor of 0.173 corresponds to providing thrust at a maximum tilt of 56.4 deg from vertical
        double total_thrust_n = vert_thrust_n / compFactor;
        //Compensate for air density
        double densityDownPull =  calcDensityDownPull(pr.airDensity);

        //Calculate the command
        //cr.thrust = (total_thrust_n  + pr.thrustCurveOffset) * pr.thrustCurveFactor + densityDownPull;
        cr.thrust = (total_thrust_n  + pr.thrustCurveOffset) * pr.thrustCurveFactor; // + densityDownPull;

        //ROS_INFO_STREAM("Thrust :"<<cr.thrust);
        //ROS_INFO_STREAM(cr.thrust<<", tt "<<total_thrust_n<<", vt "<<vert_thrust_n<<" ,comp "<<compFactor<<" ,vzIntegrator "<<ctState.vzIntegrator<<", densitydownPull "<< densityDownPull<<" , error "<<zd_err);
    } else
    {
        ROS_WARN_STREAM("Thrust Controller disabled!");
    }

    //
    // Velocity Control
    //
    if(pr.doVelocityControl)
    {
        double xd_err = command.velocity[0] - actual.velocity[0];
        double yd_err = command.velocity[1] - actual.velocity[1];
		double x_comand_change = command.velocity[0] - prev_command.velocity[0];
		double y_comand_change = command.velocity[1] - prev_command.velocity[1];

        ctState.vxIntegrator += xd_err * dt;
        ctState.vyIntegrator += yd_err * dt;

        ctState.vxIntegrator = std::max(ctState.vxIntegrator,-pr.velocityIMax);
        ctState.vxIntegrator = std::min(ctState.vxIntegrator, pr.velocityIMax);
        ctState.vyIntegrator = std::max(ctState.vyIntegrator,-pr.velocityIMax);
        ctState.vyIntegrator = std::min(ctState.vyIntegrator, pr.velocityIMax);

        double ux = pr.velocityFF*command.acceleration[0] +
                    pr.velocityP * xd_err
                    + pr.velocityI * ctState.vxIntegrator
                    + pr.velocityD * (xd_err - (ctState.prev_xd_err + x_comand_change))/dt;
        //								-velocityD*(velocity_fb[0] - prev_velocity_fb[0])/dt;
        double uy = pr.velocityFF*command.acceleration[1] +
                    pr.velocityP * yd_err
                    + pr.velocityI * ctState.vyIntegrator
                    + pr.velocityD * (yd_err - (ctState.prev_yd_err + y_comand_change))/dt;
        //								-velocityD*(velocity_fb[1] - prev_velocity_fb[1])/dt;

		if(debug)
		{
			ca_common::debugPIDcomponents debugPIDcomponents;
			debugPIDcomponents.p=pr.velocityP * xd_err;
			debugPIDcomponents.i=pr.velocityI * ctState.vxIntegrator;
			debugPIDcomponents.d=pr.velocityD * (xd_err - ctState.prev_xd_err)/dt;
			pubDebugVX.publish(debugPIDcomponents);
			//ROS_ERROR_STREAM("xd_err "<<xd_err<<" prev_xd_err "<<ctState.prev_xd_err<<" pitch_cmd_change "<<pitch_cmd_change<<" dt "<<dt<<" old "<<(xd_err - ctState.prev_xd_err)/dt<<" new "<<(xd_err - (ctState.prev_xd_err +pitch_cmd_change))/dt);
			
			debugPIDcomponents.p=pr.velocityP * yd_err;
			debugPIDcomponents.i=pr.velocityI * ctState.vyIntegrator;
			debugPIDcomponents.d=pr.velocityD * (yd_err - ctState.prev_yd_err)/dt;
			pubDebugVY.publish(debugPIDcomponents);

		}
        ctState.prev_xd_err = xd_err;
        ctState.prev_yd_err = yd_err;


        CA::Vector3D u,umapped;
        u[0] = ux;
        u[1] = uy;
        u[2] = 0.0;
        // TODO We should map from linear velocity to roll/pitch angles

        // The vehicle regards roll backward from us since is considers +z up
        // Pitch is the same though
        umapped[0] = cos(actual.heading) * u[0] + sin(actual.heading) *u[1];
        umapped[1] = -sin(actual.heading) * u[0] + cos(actual.heading) *u[1];
        //ROS_ERROR_STREAM("xd_err "<<xd_err<<" yd_err "<<yd_err <<" ux "<< ux<<" uy "<<uy<<" roll "<<umapped[0]<<" pitch "<<umapped[1] );
        //						cr.roll = asin(umapped[1])*(-1.0)*attitudeGain;
        //						cr.pitch = asin(umapped[0])*attitudeGain;



        //u = (orientation.inverse()*u).eval();
        //			roll_cmd = asin(u[1])*(-1.0)*attitudeGain;
        //			pitch_cmd = asin(u[0])*attitudeGain;
        cr.roll  = umapped[1] * (-1.0) * pr.attitudeGain;
        cr.pitch = umapped[0]          * pr.attitudeGain;

    } else
    {
        ROS_WARN_STREAM("X/Y Velocity Control disabled!");
    }

    ROS_DEBUG_STREAM_THROTTLE(1.0,"Vel ctrl int: "<<ctState.vxIntegrator<<" "<<ctState.vyIntegrator<<" "<<ctState.vzIntegrator);
    //
    // Prevent Craziness
    //
    cr.yaw    = std::min(cr.yaw,    pr.maxYawCmd      );
    cr.yaw    = std::max(cr.yaw,    pr.minYawCmd      );
    cr.roll   = std::min(cr.roll,   pr.maxAttitudeCmd );
    cr.roll   = std::max(cr.roll,   pr.minAttitudeCmd );
    cr.pitch  = std::min(cr.pitch,  pr.maxAttitudeCmd );
    cr.pitch  = std::max(cr.pitch,  pr.minAttitudeCmd );
    cr.thrust = std::min(cr.thrust, pr.maxThrustCmd   );
    cr.thrust = std::max(cr.thrust, pr.minThrustCmd   );

    return cr;

}


MkModelLibCommand CA::HavControl::levelCommand(void)
{
    MkModelLibCommand command;
    command.roll = 0;
    command.pitch = 0;
    command.yaw = 0;
//	command.thrust = (CA::math_consts::GRAVITY * pr.mass  + pr.thrustCurveOffset) * pr.thrustCurveFactor + calcDensityDownPull(pr.airDensity);
    command.thrust = (CA::math_consts::GRAVITY * pr.mass  + pr.thrustCurveOffset) * pr.thrustCurveFactor; // + calcDensityDownPull(pr.airDensity);

    return command;
}

