#include "ros/ros.h"
#include "mikrokopter/Control.h"
#include "spektrum/MikrokopterStatus.h"
#include <spk_weather_board/AirDensityHeight.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Empty.h>
#include <trajectory_control/Command.h>
#include <ca_common/math.h>
#include <cstdlib>
#include <algorithm>
#include <hav_control/hav_control_lib.h>
#include <riverine_watchdog/watchdog.h>

using namespace CA;

CA::QuatD orientation;

MkVelocityControlCommand actual;
MkVelocityControlCommand desired,prev_desired;
MkVelocityControlState controllerState;
HavControlParameters parameters;
bool isControllerAlive = true;

double control_loop_freq;
double watchdogFreq;
double CommandVelocityRange;
double OdomVelocityRange;
double lowpass_alpha;

ros::Time lastOdomTime;

void densityCallback(const spk_weather_board::AirDensityHeight::ConstPtr& msg)
{
    if((*msg).valid_density)
        parameters.airDensity = (*msg).density_rho;
}

void odometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    lastOdomTime = msg->header.stamp;
    //actual.velocity = math_tools::parentVelocity(*msg);
    //LowPassFilter
    CA::Vector3D  input_velocity = math_tools::parentVelocity(*msg);
    actual.velocity = actual.velocity+lowpass_alpha*(input_velocity-actual.velocity);
    
    orientation = CA::msgc(msg->pose.pose.orientation);
    CA::Vector3D attitude = CA::quatToEuler(orientation);
    actual.heading = attitude[2];
}

void commandCallback(const trajectory_control::Command::ConstPtr& msg)
{
    desired.velocity[0]     = msg->velocity.x;
    desired.velocity[1]     = msg->velocity.y;
    desired.velocity[2]     = msg->velocity.z;
    desired.acceleration[0] = msg->acceleration.x;
    desired.acceleration[1] = msg->acceleration.y;
    desired.acceleration[2] = msg->acceleration.z;
    desired.heading         = msg->heading;
    desired.headingrate     = msg->headingrate;

    isControllerAlive = true;

}

void resetCallback(const std_msgs::Empty::ConstPtr & msg)
{
    controllerState.reset();
    ROS_INFO_STREAM("HAV_control: Got reset request");
}

void statusCallback(const spektrum::MikrokopterStatus::ConstPtr &msg)
{
    if (!msg->isAutonomous)
    {
        controllerState.reset();
        ROS_INFO_STREAM_THROTTLE(5.0,"HAV_control: Got reset request from spektrum");
    }
}

void watchdogCallback(const ros::TimerEvent& event)
{
    static double lastHeading;

    // It will only be 0.0 if never initialized
    if (lastHeading == 0.0)
        lastHeading = actual.heading;

    if (isControllerAlive)
    {
        isControllerAlive = false;
        lastHeading = actual.heading;
    }
    else
    {
        desired.velocity[0] = 0;
        desired.velocity[1] = 0;
        desired.velocity[2] = 0;
        desired.heading     = lastHeading;
        ros::Duration dt = event.current_real-event.last_real;
        ROS_INFO_STREAM_THROTTLE(1, "HAV_controller has not received setpoints for >" << dt.toSec() << " seconds. Default to hover behavior." );
    }
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "hav_control");
    ros::NodeHandle n("hav_control");


    if(!parameters.loadParameters(n))
    {
        ROS_ERROR_STREAM("hav_control: Was not able to get all initial parameters");
        return -1;
    }

    n.param("watchdogFreq", watchdogFreq, 2.0);
    n.param("control_loop_freq", control_loop_freq, 25.0);
    n.param("validInputCommandVelocityRange", CommandVelocityRange, 0.0);
    n.param("validInputOdomVelocityRange", OdomVelocityRange, 0.0);
    n.param("lowpass_alpha", lowpass_alpha, 1.0);

    if (control_loop_freq < 1.0)
    {
        ROS_ERROR_STREAM("HAV_controller set to control at " << control_loop_freq << " hz. This is too slow. Set to default rate: 25Hz.");
        control_loop_freq = 25.0;
    }

    ros::Publisher pub = n.advertise<mikrokopter::Control>("/mikrokopter/req_set_control", 50);

    ros::TransportHints hints = ros::TransportHints().udp().tcpNoDelay();
    ros::Subscriber  airdensity_sub  = n.subscribe<spk_weather_board::AirDensityHeight>("density", 1, densityCallback);
    ros::Subscriber  odometry_fb_sub = n.subscribe<nav_msgs::Odometry>("odometry", 1, odometryCallback, hints);
    ros::Subscriber command_sub      = n.subscribe<trajectory_control::Command>("command", 1,commandCallback, hints);
    ros::Subscriber reset_sub        = n.subscribe<std_msgs::Empty>("reset", 1, resetCallback);
    ros::Subscriber status_sub       = n.subscribe<spektrum::MikrokopterStatus>("/spektrum/status", 1, statusCallback);

    ros::Timer wdt = n.createTimer(ros::Duration(1/watchdogFreq), watchdogCallback);
	
    //lowpass
    actual.velocity[0]=0;
    actual.velocity[1]=0;
    actual.velocity[2]=0;
    
    mikrokopter::Control req;
    req.digital[0] = 0;
    req.digital[1] = 0;
    req.remoteKey = 0;
    req.pitch = 0;
    req.roll = 0;
    req.yaw = 0;
    req.thrust = 250;
    req.height = 0;
    req.free = 0;
    req.frame = 1;
    req.config = 1;
    if(parameters.doThrustControl) ROS_INFO_STREAM("Thrust Control Enabled");
    else ROS_WARN_STREAM("Thrust Control DISABLED");

    if(parameters.doHeadingControl) ROS_INFO_STREAM("Heading Control Enabled");
    else ROS_WARN_STREAM("Heading Control DISABLED");

    if(parameters.doVelocityControl) ROS_INFO_STREAM("Velocity Control Enabled");
    else ROS_WARN_STREAM("Velocity Control DISABLED");

    HavControl control(parameters);
    control.debugInit(n);
    CA::PetWatchdog pet;
    if(!pet.setup(n))
    {
        ROS_ERROR_STREAM("Was not able to setup watchdog");
        return -1;
    }

    ros::Rate loop_rate(control_loop_freq);
    while (ros::ok())
    {
        ros::spinOnce();
        double dt = 1.0/control_loop_freq;

        double timediff = ros::Time::now().toSec() - lastOdomTime.toSec();

        MkModelLibCommand command;

        if(timediff <= 1.0)
        {
            if(control.validVelocityRange(OdomVelocityRange,CommandVelocityRange,actual,desired))
            {
                command = control.velocityControl(dt,orientation,controllerState,actual,desired,prev_desired);
                prev_desired=desired;
            }

            else
            {
                pet.fault();
                command = control.levelCommand();
            }
        }
        else
        {
            ROS_ERROR_STREAM_THROTTLE(0.1, "HAV_control: Odometry is more than one second old, level command issued" << timediff);
            pet.fault();
            command = control.levelCommand();
        }

        req.frame++;
        req.header.stamp = ros::Time::now();
        req.roll = command.roll;
        req.pitch = command.pitch;
        req.yaw = command.yaw;
        req.thrust = command.thrust;
        pub.publish(req);


        if(!std::isfinite(dt)|| !isfinite(orientation) || !controllerState.isfinite() || !actual.isfinite() || !desired.isfinite())
        {
            ROS_ERROR_STREAM_THROTTLE(1.0,"Received bad commands");
            pet.fault();
        }
        else
            pet.alive();


        loop_rate.sleep();
    }
    return 0;
}


