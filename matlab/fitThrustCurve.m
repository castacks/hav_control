%% Import the data
gravity = 9.81
basemass = 4.1 * gravity
battery = 0.6 * gravity
T = basemass + battery * [1 2 3 ];
%vars = {'time','field.gas'};
S = load('hover1b.mat','data');
hover1 = S.data(:,12);
S = load('hover2c.mat','data');
hover2 = S.data(:,12);
S = load('hover3.mat','data');
hover3 = S.data(:,12);
S = load('hover2NoLaser.mat','data');
hover4 = S.data(:,12);
clear S
% Remove irrelvant parts:
%hover1=hover1(1:84);
hover2=hover2(73:503);
hover3=hover3(57:271);
%hover4=hover4(11:end);
%% Calculate the hover thrusts for the three conditions:
figure(1);
plot(hover1,'.')
figure(4)
hist(hover1,20)
%%
figure(2);
plot(hover2,'.')
figure(5)
hist(hover2,20)
%%
figure(3);
plot(hover3,'.')
figure(6)
hist(hover3,20)
%%
%figure(10);
%plot(hover4,'.');
%figure(11);
%hist(hover4,20);
%% Median & Mean
meanCommand = [mean(hover1) mean(hover2) mean(hover3) ]
medianCommand = [ median(hover1) median(hover2) median(hover3) ]
figure(7)
boxplot(hover1)
figure(8)
boxplot(hover2)
figure(9)
boxplot(hover3)

%% command vs. thrust:
figure(1);
plot(T,medianCommand/4,'.')
hold on;
x=T(1):0.1:T(end);
%y=3.0 * x -3;
%plot(x,y,'-g');
yn = 2.39 * x +18;
plot(x,yn,'-b');
hold off;
