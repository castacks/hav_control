%% Gather all the data for the different flights
thrustTableData = [
   %Thrust | Newton | Density
%Estimate from openloop
    134      46.10    0.95 
    154      51.99    0.95
    162      57.87    0.95
%From hover experiment
    158      48.00    0.61
%   
    163      50.81    0.71
    163      50.81    0.75
    163      50.81    0.69
    163      50.81    0.69
    166      50.81    0.60
% 2012/03/17
    159      47.58    1.07
    ]; 
          
%% Plot the data and fit a surface
figure(1);
plot(thrustTableData(:,1),thrustTableData(:,3),'r.-')